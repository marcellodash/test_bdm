#ifndef CONFIG_H
#define CONFIG_H


#define READ_SIZE (1024*1024)
#define BLOCK_SIZE_MIN (64)
#define BLOCK_SIZE_MAX (32*1024)

// Load one of these Block Disks
#define LOAD_BD_USB
#define LOAD_BD_SIO2SD
//#define LOAD_BD_IEEE

// Load one or more File Systems
#define LOAD_FS_VFAT

// Where to run the tests
#define TEST_ON_EE
#define TEST_ON_IOP


#endif
