#include <dirent.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <malloc.h>

// PS2SDK header files
#include <loadfile.h>

// Project header files
#include "config.h"

#define IRX_DEFINE(mod)                                                        \
  extern unsigned char mod##_irx[];                                            \
  extern unsigned int size_##mod##_irx

#define IRX_LOAD(mod)                                                          \
  if (SifExecModuleBuffer(mod##_irx, size_##mod##_irx, 0, NULL, NULL) < 0)     \
  printf("Could not load ##mod##\n")

IRX_DEFINE(iomanX);
IRX_DEFINE(fileXio);
IRX_DEFINE(bdm);
IRX_DEFINE(udptty);
IRX_DEFINE(usbd);
IRX_DEFINE(usbmass_bd);
IRX_DEFINE(sio2man);
IRX_DEFINE(sio2sd_bd);
IRX_DEFINE(iLinkman);
IRX_DEFINE(IEEE1394_bd);
IRX_DEFINE(bdmfs_vfat);

extern void fileXioInit();

//--------------------------------------------------------------
int dir_exists(const char *dirname) {
  DIR *pDir = opendir(dirname);
  if (pDir == NULL) {
    return 0;
  }
  else {
    closedir(pDir);
    return 1;
  }
}

//--------------------------------------------------------------
int list_files(const char *dirname) {
  struct dirent *pDirent;
  DIR *pDir;

  pDir = opendir(dirname);
  if (pDir == NULL) {
    printf("Cannot open directory '%s'\n", dirname);
    return -1;
  }

  while ((pDirent = readdir(pDir)) != NULL) {
    struct stat st;
    char filename[80];
    char *stime;
    mode_t m;
    struct tm *timeinfo;

    snprintf(filename, 80, "%s/%s", dirname, pDirent->d_name);
    if (stat(filename, &st) < 0) {
      printf("%s\n", filename);
      continue;
    }
    m = st.st_mode;
    timeinfo = localtime(&st.st_mtime);

    stime = asctime(timeinfo);
    stime[24]=0;
    printf("%c%c%c%c%c%c%c%c%c%c %s %6llu %s\n", (m & S_IFDIR) != 0 ? 'd' : '-',
           (m & S_IRUSR) != 0 ? 'r' : '-', (m & S_IWUSR) != 0 ? 'w' : '-',
           (m & S_IXUSR) != 0 ? 'x' : '-', (m & S_IRGRP) != 0 ? 'r' : '-',
           (m & S_IWGRP) != 0 ? 'w' : '-', (m & S_IXGRP) != 0 ? 'x' : '-',
           (m & S_IROTH) != 0 ? 'r' : '-', (m & S_IWOTH) != 0 ? 'w' : '-',
           (m & S_IXOTH) != 0 ? 'x' : '-', stime, st.st_size,
           pDirent->d_name);
  }
  closedir(pDir);
  return 0;
}

//--------------------------------------------------------------
void read_test(const char * filename, unsigned int buf_size, unsigned int max_size)
{
    int fd_size, size_left, msec, fd;
    char * buffer = NULL;
    clock_t clk_start, clk_end;

	if ((fd = open(filename, O_RDONLY)) <= 0) {
		printf("Could not find '%s'\n", filename);
		return;
	}

	fd_size = max_size;

	buffer = malloc(buf_size);

	clk_start = clock();
	size_left = fd_size;
	while (size_left > 0) {
        int read_size = (size_left > buf_size) ? buf_size : size_left;
        if (read(fd, buffer, read_size) != read_size) {
            printf("Failed to read file.\n");
            return;
        }
        size_left -= read_size;
	}
    clk_end = clock();

	msec = (int)((clk_end - clk_start) * 1000 / CLOCKS_PER_SEC);
	printf("Read %dKiB in %dms, blocksize=%d, speed=%dKB/s\n", fd_size/1024, msec, buf_size, fd_size / msec);

	free(buffer);

	close(fd);
}

//--------------------------------------------------------------
void read_tests(const char * filename)
{
  int buf_size;

  printf("\nStarting speed test on %s...\n", filename);
  for (buf_size = BLOCK_SIZE_MIN; buf_size <= BLOCK_SIZE_MAX; buf_size *= 2)
    read_test(filename, buf_size, READ_SIZE);
}

//--------------------------------------------------------------
int main() {
  printf("BDM (Block Device Manager) test application\n");

  printf("Loading iomanX and fileXio IOP modules\n");
  IRX_LOAD(iomanX);
  IRX_LOAD(fileXio);
  fileXioInit();

  printf("Loading BDM IOP modules\n");
  IRX_LOAD(bdm);
#ifdef LOAD_BD_USB
  IRX_LOAD(usbd);
  IRX_LOAD(usbmass_bd);
#endif
#ifdef LOAD_BD_SIO2SD
  IRX_LOAD(sio2man);
  IRX_LOAD(sio2sd_bd);
#endif
#ifdef LOAD_BD_IEEE
  IRX_LOAD(iLinkman);
  IRX_LOAD(IEEE1394_bd);
#endif
#ifdef LOAD_FS_VFAT
  IRX_LOAD(bdmfs_vfat);
#endif

  printf("Wait 5s for drives");
  nanosleep((const struct timespec[]){{5, 0}}, NULL);

  if(dir_exists("mass0:")) {
    printf("\n\nListing files in mass0:\n");
    list_files("mass0:");
    read_tests("mass0:zero.bin");

    if(dir_exists("mass1:")) {
        printf("\n\nListing files in mass1:\n");
        list_files("mass1:");
        read_tests("mass1:zero.bin");
    }
  }
  else {
    printf("\n\nNo compatible devices found\n");
  }


  printf("Done. Exit in 5s\n");
  nanosleep((const struct timespec[]){{5, 0}}, NULL);

  return 0;
}
